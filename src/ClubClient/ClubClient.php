<?php


namespace client\ClubClient;

class ClubClient
{
    const QUEUE_NAME = 'club_queue';
    const INCREASE_TYPE = 'deposit';
    const DECREASE_TYPE = 'withdraw';

    /**
     * @var Amqp
     */
    protected $amqp;

    public function __construct()
    {
        $amqp = new Amqp();
        $this->amqp = $amqp;
    }

    /**
     * @param int $userId
     * @param int $point
     * @param int $reasonId
     * @param string $description
     * @throws \Exception
     */
    public function increasePoint(int $userId, int $point, int $reasonId, string $description)
    {
        $message = ['user_id'=> $userId,  'point'=> $point, 'reason_id' => $reasonId ,'description' => $description , 'type' => self::INCREASE_TYPE];
        $message = json_encode($message);
        $this->producer($message);
    }

    /**
     * @param int $userId
     * @param int $point
     * @param int $reasonId
     * @param string $description
     * @throws \Exception
     */
    public function decreasePoint(int $userId, int $point, int $reasonId, string $description)
    {
        $message = ['user_id'=> $userId,  'point'=> $point, 'reason_id' => $reasonId ,'description' => $description , 'type' => self::DECREASE_TYPE];
        $message = json_encode($message);
        $this->producer($message);
    }

    /**
     * @param $message
     * @throws \Exception
     */
    private function producer($message)
    {
        $this->amqp->amqpProducer(self::QUEUE_NAME, $message);
    }

}