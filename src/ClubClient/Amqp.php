<?php


namespace client\ClubClient;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;



class Amqp
{

    /**
     * @param $queue_name
     * @param $message
     * @param null $host
     * @param null $port
     * @param null $username
     * @param null $password
     * @param null $vhost
     * @throws \Exception
     */
    public function amqpProducer($queue_name, $message, $host = null, $port = null, $username = null, $password = null, $vhost = null)
    {
        if ($host != null) {
            $connection = new AMQPStreamConnection($host, $port, $username, $password, $vhost);
        } else {
            $connection = new AMQPStreamConnection(

                env('AMQP_HOST'),
                env('AMQP_PORT'),
                env('AMQP_USERNAME'),
                env('AMQP_PASSWORD'),
                env('AMQP_VHOST')
            );
        }

        $channel = $connection->channel();
        $channel->queue_declare($queue_name, false, true, false, false);

        $message = new AMQPMessage($message, array('content_type' => 'application/json', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $channel->basic_publish($message, '', $queue_name);

        $channel->close();
        $connection->close();
    }

    /**
     * @param $queue_name
     * @param $callback
     * @param null $host
     * @param null $port
     * @param null $username
     * @param null $password
     * @param null $vhost
     * @throws \ErrorException
     */
    public function amqpConsumer($queue_name, $callback, $host = null, $port = null, $username = null, $password = null, $vhost = null)
    {

        if ($host != null) {
            $connection = new AMQPStreamConnection($host, $port, $username, $password);
        } else {
            // config connection
            $connection = new AMQPStreamConnection(

                env('AMQP_HOST'),
                env('AMQP_PORT'),
                env('AMQP_USERNAME'),
                env('AMQP_PASSWORD'),
                env('AMQP_VHOST')
            );
        }

        $channel = $connection->channel();
        $channel->queue_declare($queue_name, false, true, false, false);

        $channel->basic_consume($queue_name, '', false, false, false, false, $callback);

        while ($channel->is_consuming()) {
            $channel->wait();
        }
    }
}
