<?php


namespace client\Providers;

use client\Console\ClientGenerateConfigCommand;
use Illuminate\Support\ServiceProvider;

abstract class AbstractServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    abstract public function boot();

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerJWTCommand();
        $this->commands('salli.client.config');
    }


    /**
     * Register the Artisan command.
     *
     * @return void
     */
    protected function registerJWTCommand()
    {
        $this->app->singleton('salli.client.config.', function () {
            return new ClientGenerateConfigCommand();
        });
    }


}