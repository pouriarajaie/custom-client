<?php


namespace client\Providers;


class LumenServiceProvider extends AbstractServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->app->configure('client');

        $path = realpath(__DIR__ . '/../../config/config.php');
        $this->mergeConfigFrom($path, 'client');

    }

}