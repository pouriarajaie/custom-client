<?php


namespace client\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ClientGenerateConfigCommand extends Command
{
    /**
     * Directories mode
     */
    const mode = 0777;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create client config';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        if (!File::ensureDirectoryExists(base_path('config'))) {
            File::makeDirectory(base_path('config'), self::mode, true, true);
            $this->info('config dir added');
        }
    }
}