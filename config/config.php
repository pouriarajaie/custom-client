<?php

return [
    'AMQPConnection'=>[
        'AMQP_HOST' => env('AMQP_HOST' ,'localhost'),
        'AMQP_PORT' => env('AMQP_PORT',5672),
        'AMQP_USERNAME' =>  env('AMQP_USERNAME', 'guest'),
        'AMQP_PASSWORD' => env('AMQP_PASSWORD', 'guest'),
        'AMQP_VHOST' => env('AMQP_VHOST', '/')
        ],
];
